package com.lzm.toolfx.controller;

import com.lzm.toolfx.utils.SysInfo;
import de.felixroske.jfxsupport.FXMLController;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ProgressIndicator;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@FXMLController
public class PerformanceController implements Initializable {
    @FXML
    ProgressIndicator pi_mem;
    @FXML
    ProgressIndicator pi_net;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        scheduleService();
    }
    private void scheduleService(){
        ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
        service.scheduleAtFixedRate(()->{
            double total = SysInfo.getSystemInfo().getMemory().getTotal()/1073741824;
            double available = SysInfo.getSystemInfo().getMemory().getAvailable()/1073741824;
            double percent = (available/total);
            pi_mem.setProgress(percent);
            pi_net.setProgress(percent);
        },0,500, TimeUnit.MILLISECONDS);
    }
}

package com.lzm.toolfx.common;

import com.alibaba.fastjson.JSONObject;
import com.lzm.toolfx.ToolFxApplication;
import org.yaml.snakeyaml.Yaml;

import java.io.FileInputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public final class Property {
    /**BANNER*/
    public static final String BANNER =
                    "Author: LZM \nCopyright® 2020";

    public static final String dash = "————————————————————————————————————————————————————";
    public static final String dot =  "****************************************************";

    public static final List<String> baudRate = new ArrayList<>(Arrays.asList("4800", "9600", "14400","19200","38400","56000","57600","115200"));
    public static final String mb_dash = "\n—————————————————————————————————————————\n";

    /**版本号*/
    public static String VERSION;
    /**服务器IP地址*/
    private static String server_host;
    /**服务器端口号*/
    public static String server_port;
    /**管理员密码*/
    public static String server_password;
    /**是否展示banner*/
    public static boolean showBanner;
    /**是否锁定程序*/
    public static boolean isLock;
    /**Sql 数据库可用*/
    public static boolean DBAvailable;
    /**MQ 消息队列可用*/
    public static boolean MQAvailable;


    /**数据库配置*/
    public static String db_ip;
    public static String db_port;
    public static String db_userName;
    public static String db_password;
    public static String db_name;//数据库名

    /**消息队列配置*/
    public static String msmq_ip;
    public static String msmq_queueName;


    /**
     * @return  host 服务器IP地址
     */
    public String getHost() { return server_host;}

    /**
     * @param   host 服务器地址
     */
    public void setHost(String host) { this.server_host = host;}

    /**
     * @return  host 服务器IP地址
     */
    public String getPort() { return server_port;}

    /**
     * @param   port 服务器端口号
     */
    public void setPort(String port) { this.server_port = port;}

    /**
     * @return  host 是否展示Banner
     */
    public boolean isShowBanner() { return showBanner; }

    /**
     * @param   showBanner 设置是否展示品牌信息
     */
    public void setShowBanner(boolean showBanner) { this.showBanner = showBanner; }

    public static final String IS_CLOSE = "ClOSE";
    public static final String IS_OPEN = "OPEN";

    /**执行读取操作*/
    public void execute(){
        try {
            Yaml yaml = new Yaml();
            URL url = ToolFxApplication.class.getClassLoader().getResource("application.yml");
            if (url != null) {
                //获取yml文件配置数据，转换为JSON，
                Map<String, Object> map = yaml.load(new FileInputStream(url.getFile()));
                JSONObject property = new JSONObject(map);

                JSONObject database = property.getJSONObject("database");
                JSONObject server = property.getJSONObject("server");
                JSONObject MSMQ = property.getJSONObject("msmq");

                /*db_ip = database.getString("ip");
                db_port = database.getString("port");
                db_name = database.getString("dbName");
                db_userName = database.getString("userName");
                db_password = database.getString("password");*/

               /* msmq_ip = MSMQ.getString("ip");
                msmq_queueName = MSMQ.getString("queueName");*/

                VERSION = server.getString("version");
                server_port = server.getString("port");
                showBanner = server.getBoolean("showBanner");
                server_password = server.getString("password");
                isLock = server.getBoolean("isLock");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}

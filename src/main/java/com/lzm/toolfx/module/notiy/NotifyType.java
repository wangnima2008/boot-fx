package com.lzm.toolfx.module.notiy;

public enum NotifyType {
    IS_CLOSE,       //关闭
    IS_CONNECT,     //连接
    SIZE            //大小
}

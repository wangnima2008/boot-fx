package com.lzm.toolfx.module.notiy;

import javafx.scene.layout.AnchorPane;

public class FakeSocket extends AnchorPane {
    public String ip;
    public int size  = 0;
    public int deviceId ;
    private long time;
    public boolean connected;

    public FakeSocket(String ip){
        this.ip = ip;
    }
    public FakeSocket(String ip, int size) {
        this(ip);
        this.size = size;
    }
    public FakeSocket(String ip,boolean connected){
        this(ip);
        this.connected = connected;
    }
    public FakeSocket(String ip,int size,boolean connected){
        this(ip,size);
        this.connected = connected;
    }
    public FakeSocket(String ip,int size,long time){
        this(ip,size);
        this.time = time;
    }
    public FakeSocket(String ip,int size,long time,int deviceId){
        this(ip,size,time);
        this.deviceId = deviceId;
    }


    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(int deviceId) {
        this.deviceId = deviceId;
    }
}

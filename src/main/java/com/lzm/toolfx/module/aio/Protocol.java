package com.lzm.toolfx.module.aio;

import java.nio.ByteBuffer;

/**
 * 解析数据采用的协议
 * @param <T> 消息对象实体
 **/
public interface Protocol<T> {
    /**
     * 从Socket inputStream 获取流的处理
     * @param buffer 接受到的流
     * @param server AioServer 实体类，用于定位SocketChannel，确保线程安全
     */
    T  decode(final ByteBuffer buffer, AioServer server);
}

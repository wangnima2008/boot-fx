package com.lzm.toolfx.utils;

import com.alibaba.fastjson.JSONObject;
import java.awt.*;
import java.io.*;

public class ConfigUtils {
    private static String config = "config.config";
    private static JSONObject configJSON = new JSONObject();
    private static Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

    public static void checkConfig(){
        File configFile = new File(config);
        try {
            FileWriter writer=new FileWriter(config);
            BufferedReader in = new BufferedReader(new FileReader(config));
            if(!configFile.exists()) {
                configFile.createNewFile();
            }else {
                configJSON.put("width",screenSize.width);
                configJSON.put("height",screenSize.height);
                writer.write(configJSON.toJSONString());
                writer.close();
                StringBuilder builder = new StringBuilder();
                String str;
                while ((str = in.readLine()) != null) {
                    builder.append(str);
                }
                configJSON = JSONObject.parseObject(builder.toString());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Dimension getScreenSize() {
        return screenSize;
    }
}

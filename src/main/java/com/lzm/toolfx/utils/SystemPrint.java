package com.lzm.toolfx.utils;

import javafx.scene.control.TextArea;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;


/**
 * 重定向System.out.print()的输出流到TextArea
 * */
public class SystemPrint extends PrintStream {
    TextArea console;

    public SystemPrint(TextArea console) {
        super(new ByteArrayOutputStream());
        this.console = console;
    }

    @Override
    public void write(byte[] buf, int off, int len) {
        /*if(console.getText().length()>5000){
            console.clear();
        }*/
        print(new String(buf, off, len, StandardCharsets.UTF_8));

    }

    @Override
    public void print(String s) {
        console.appendText(s);
    }
}

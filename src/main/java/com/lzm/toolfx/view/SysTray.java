package com.lzm.toolfx.view;

import lombok.Data;
import org.springframework.stereotype.Component;
import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

/**SystemTray wrapper class
 * @author LIANG
 * @since version 1.0 2020-11
*/
@Data
@Component
public class SysTray {
    BufferedImage image;
    TrayIcon trayIcon;
    public SysTray() throws IOException {
        image = ImageIO.read(SysTray.class
                .getResourceAsStream("/static/img/icon.png"));
        trayIcon = new TrayIcon(image, "BLTec");
        trayIcon.setImageAutoSize(true);
        trayIcon.setToolTip("BLTec");
    }

}

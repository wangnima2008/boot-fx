package com.lzm.toolfx.view;

import de.felixroske.jfxsupport.SplashScreen;

/**Splash when program launching
 * @author LIANG
 * @since version 1.0 2020-11
 */
public class Splash extends SplashScreen {
    //Default image path allows: *.png *.jpg *.gif *.jpeg *.bmp
    private String IMAGE_PATH = "/static/img/start.gif";
    @Override
    public boolean visible() {
        return false;
    }
    @Override
    public String getImagePath() {
        return IMAGE_PATH;
    }

    public void setImage(String imagePath) {
        this.IMAGE_PATH = imagePath;
    }

    public static Splash getSplash(){
        return new Splash();
    }
}

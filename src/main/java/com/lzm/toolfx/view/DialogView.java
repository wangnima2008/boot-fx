package com.bltec.toolfx.view;

import com.bltec.toolfx.utils.NodeUtil;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DialogPane;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/*自定义Dialog*/
public class DialogView extends Stage {
    //initialize Dialog Style
    public enum DialogStyle{
        MAX,
        MIN,
        CUSTOM_MAX,
        CUSTOM_MIN
    }
    private Stage owner;//Parent Stage;
    private DialogPane pane;//ContentPanel;
    private Scene scene;
    private Node maxView;
    private Node minView;
    private static ImageView min = NodeUtil.getPic("/static/img/dialog/min.png",20);
    private static ImageView max = NodeUtil.getPic("/static/img/dialog/full.png",50);
    private static ImageView close = NodeUtil.getPic("/static/img/dialog/close.png",20);
    public static ImageView iv_Full = NodeUtil.getPic("/static/img/dialog/full_blank.png",40);
    public static ImageView iv_exitFull = NodeUtil.getPic("/static/img/dialog/exit_full.png",40);
    private static Button fullScreen = NodeUtil.getPicBtn("全屏","/static/img/dialog/full_blank.png",40);
    private static double xOffset;
    private static double yOffset;

    public void setContent(DialogStyle style){
        switch (style){
            case MAX:
                pane.setContent(getVBox());
                break;
            case MIN:
                pane.setContent(getMax());
                break;
            case CUSTOM_MAX:
                pane.setContent(getMaxView());
                break;
            case CUSTOM_MIN:
                pane.setContent(getMinView());
                break;
        }

    }
    public DialogView(Stage owner, StageStyle stageStyle, Modality modality) {
        initOwner(owner);
        initStyle(stageStyle);
        initModality(modality);
        this.owner = owner;
        setWidth(200);
        setHeight(200);
        setResizable(false);
        pane = new DialogPane();
        pane.setMaxSize(200,200);
        pane.setStyle("-fx-background-color: #00000000");
        scene = new Scene(pane);
        scene.setFill(null);
        setScene(scene);
        setContent(DialogStyle.MAX);
        initMove();
    }
    public void showDialog() {
        owner.setIconified(true);
        sizeToScene();
        setAlwaysOnTop(true);
        show();
    }

    private VBox getVBox(){
        VBox vBox = new VBox();
        vBox.setSpacing(10);
        vBox.setAlignment(Pos.CENTER);
        HBox hBox = new HBox();
        hBox.setSpacing(5);
        hBox.setAlignment(Pos.TOP_RIGHT);
        hBox.getChildren().add(min);
        hBox.getChildren().add(close);
        GridPane grid = new GridPane();
        grid.setStyle("-fx-border-color: #8a8a8a");
        grid.setStyle("-fx-background-color: #cccccc20");
        grid.setHgap(5);
        grid.setVgap(5);
        grid.setPadding(new Insets(10, 10, 10, 10));

       /* grid.add(NodeUtil.getFullScreenBtn("全屏1"),0,0);
        grid.add(NodeUtil.getFullScreenBtn("全屏2"),0,1);*/

        vBox.setStyle("-fx-background-color: #00000020");
        vBox.getChildren().add(hBox);
        vBox.getChildren().add(fullScreen);
        vBox.getChildren().add(grid);
        return vBox;
    }

    public ImageView getMin() { return min; }

    public ImageView getMax() { return max; }

    public ImageView getClose() { return close; }

    public Button getFullScreen() { return fullScreen; }

    public Scene getTheScene() { return scene; }

    public void setSize(int width,int height){
        setWidth(width);
        setHeight(height);
    }

    private void initMove(){
        pane.setOnMousePressed(event -> {
            xOffset = getX() - event.getScreenX();
            yOffset = getY() - event.getScreenY();
            pane.setCursor(Cursor.CLOSED_HAND);
        });
        pane.setOnMouseDragged(event -> {
            setX(event.getScreenX() + xOffset);
            setY(event.getScreenY() + yOffset);
        });
        pane.setOnMouseReleased(event -> pane.setCursor(Cursor.DEFAULT));
    }

    public Node getMaxView() { return maxView; }

    public void setMaxView(Node maxView) { this.maxView = maxView; }

    public Node getMinView() { return minView; }

    public void setMinView(Node minView) { this.minView = minView; }

    public void setOwnerIconified(boolean iconified){ owner.setIconified(iconified); }

    public Stage getTheOwner() { return owner; }
}
